package io.cvaldez.controller;

import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/rest")
public class Controller {
  
  private static final Logger log = LoggerFactory.getLogger(Controller.class);

  @GetMapping(value = "/countries")
  public List<Object> getCountries() throws IOException {
    Request req = new Request.Builder()
        .url("https://restcountries.eu/rest/v2/all")
        .header("Content-Type", "application/json")
        .build();
    Call call = new OkHttpClient().newCall(req);
    Response res = call.execute();
    log.info("result call client {} {}", res.code(), res.isSuccessful());
    log.info(res.body().contentType().toString());
    String content = res.body().string();
    JacksonJsonParser parser = new JacksonJsonParser();
    /// parser.parseMap(content);
    return parser.parseList(content);
  }
  
  @GetMapping(value = "/random")
  public Object genRandom() throws IOException {
    Request req = new Request.Builder()
        .url("https://randomuser.me/api/")
        .build();
    Call call = new OkHttpClient().newCall(req);
    Response res = call.execute();
    String content = res.body().string();
    JacksonJsonParser parser = new JacksonJsonParser();
    return parser.parseMap(content);
  }

}
